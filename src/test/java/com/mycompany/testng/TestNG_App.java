package com.mycompany.testng;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author adrie
 */
public class TestNG_App {

    WebDriver driver;

    @BeforeClass
    public void setUp() {
        // Spécifiez le chemin vers votre driver (dans cet exemple, ChromeDriver)
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\adrie\\OneDrive\\Documents\\Module - 03\\AppWebOne\\ChromeDriver-64\\chromedriver.exe");
        driver = new ChromeDriver();
    }

    @Test
    public void accessSiteTest() {
        // Ouvrir le site web
        driver.get("https://www.kibo.mg/");

        String pageTitle = driver.getTitle();
        System.out.println("Le titre de la page est : " + pageTitle);

        //Ecriver Chocolat dans la barre de recherche
        driver.findElement(By.className("ui-autocomplete-input")).sendKeys("Chocolat");

        //Cliquer Rechercher
        driver.findElement(By.xpath("//*[@id=\"search_widget\"]/form/button")).click();

        //Cliquer la premier résultats
        driver.findElement(By.xpath("//*[@id=\"js-product-list\"]/div[1]/article[1]/div/div[1]/div/a[2]")).click();

        //Ajouter +1 la quantité
        driver.findElement(By.xpath("//*[@id=\"add-to-cart-or-refresh\"]/div[3]/div/div[1]/div/span[3]/button[1]")).click();

        // Cliquer commander
        driver.findElement(By.xpath("//*[@id=\"add-to-cart-or-refresh\"]/div[3]/div/div[2]/button")).click();

        // COMMANDER
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.xpath("/html/body/div[3]/div/div/div[2]/div/div[2]/div/div/a")).click();

        String nombre = driver.findElement(By.xpath("//*[@id=\"main\"]/div[1]/div[1]/div[1]/div[2]/ul/li/div/div[3]/div/div[2]/div/div[1]/div/input")).getAttribute("value");
        String article = driver.findElement(By.className("label_name")).getAttribute("value");
        System.out.println(article + " ::: " + nombre);
        //d.findElement(By.className("ui-autocomplete-input")).sendKeys(a);
        /**
         * ***
         */
        // Capturer la capture d'écran en tant qu'objet BufferedImage
        BufferedImage screenshot = null;
        try {
            screenshot = ImageIO.read(((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE));
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Définir le chemin de destination de la capture d'écran
        String destinationPath = "C:\\Users\\adrie\\OneDrive\\Documents\\Module - 03\\AppWebOne\\ChromeDriver-64\\screenshot.png";

        // Enregistrer la capture d'écran dans le dossier de destination
        try {
            ImageIO.write(screenshot, "png", new File(destinationPath));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @AfterClass
    public void tearDown() {
        // Fermer le navigateur à la fin du test

    }
}
