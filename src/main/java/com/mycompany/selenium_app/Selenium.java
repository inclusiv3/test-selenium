/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.selenium_app;

import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 *
 * @author adrie
 */
public class Selenium {
    public static void main(String[] args) {
        
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\adrie\\OneDrive\\Documents\\Module - 03\\AppWebOne\\ChromeDriver-64\\chromedriver.exe");
        /*
        WebDriver d = new ChromeDriver();
        d.get("https://www.linkedin.com");
        d.findElement(By.id("session_key")).sendKeys("kiyae2123@gmail.com");
        d.findElement(By.id("session_password")).sendKeys("Aeroplane");
        d.findElement(By.xpath("//*[@id=\"main-content\"]/section[1]/div[2]/form/button")).click();
        
        String u = d.getCurrentUrl();
        if (u.equals("https://www.linkedin.com/feed/?trk=homepage-basic_signin-form_submit")){
            System.out.println("Test Case Passed");
        } else {
            System.out.println("Test Case Failed");
        }
        
        d.close();*/
        
        WebDriver d = new ChromeDriver();
        
        // Entrer dans la site kibo.mg
        d.get("https://www.kibo.mg");
        
        //Ecriver Chocolat dans la barre de recherche
        d.findElement(By.className("ui-autocomplete-input")).sendKeys("Chocolat");
        
        //Cliquer Rechercher
        d.findElement(By.xpath("//*[@id=\"search_widget\"]/form/button")).click();
        
        //Cliquer la premier résultats
        d.findElement(By.xpath("//*[@id=\"js-product-list\"]/div[1]/article[1]/div/div[1]/div/a[2]")).click();
        
        //Ajouter +1 la quantité
        d.findElement(By.xpath("//*[@id=\"add-to-cart-or-refresh\"]/div[3]/div/div[1]/div/span[3]/button[1]")).click();
        
        // Cliquer commander
        d.findElement(By.xpath("//*[@id=\"add-to-cart-or-refresh\"]/div[3]/div/div[2]/button")).click();
        
        // COMMANDER
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        d.findElement(By.xpath("/html/body/div[3]/div/div/div[2]/div/div[2]/div/div/a")).click();
        
        
        String nombre = d.findElement(By.xpath("//*[@id=\"main\"]/div[1]/div[1]/div[1]/div[2]/ul/li/div/div[3]/div/div[2]/div/div[1]/div/input")).getAttribute("value");
        String article = d.findElement(By.className("label_name")).getAttribute("value");
        System.out.println(article + " ::: " + nombre);
        //d.findElement(By.className("ui-autocomplete-input")).sendKeys(a);
        if(article.contains("CHOCOLAT") && nombre.contains("2")){
            /******/
            // Capturer la capture d'écran en tant qu'objet BufferedImage
            BufferedImage screenshot = null;
            try {
                screenshot = ImageIO.read(((TakesScreenshot) d).getScreenshotAs(OutputType.FILE));
            } catch (Exception e) {
                e.printStackTrace();
            }

            // Définir le chemin de destination de la capture d'écran
            String destinationPath = "C:\\Users\\adrie\\OneDrive\\Documents\\Module - 03\\AppWebOne\\ChromeDriver-64\\screenshot.png";

            // Enregistrer la capture d'écran dans le dossier de destination
            try {
                ImageIO.write(screenshot, "png", new File(destinationPath));
            } catch (Exception e) {
                e.printStackTrace();
            }
            
            System.out.println("YES YES");
        } else {
            System.out.println("NO NO NO");
        }
    }
    
}
